package com.kmd.bitchlasagna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitchlasagnaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitchlasagnaApplication.class, args);
	}

}
