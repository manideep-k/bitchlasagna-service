package com.kmd.bitchlasagna.LoginAuth.Dao;

import com.kmd.bitchlasagna.LoginAuth.Repo.LoginUsers;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface LoginDao {
    public List<LoginUsers> getLoginDetails() throws Exception;

}
