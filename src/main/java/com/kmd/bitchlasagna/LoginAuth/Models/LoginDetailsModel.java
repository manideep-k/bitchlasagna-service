package com.kmd.bitchlasagna.LoginAuth.Models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class LoginDetailsModel {
    public String loginId;
    public String password;
}
