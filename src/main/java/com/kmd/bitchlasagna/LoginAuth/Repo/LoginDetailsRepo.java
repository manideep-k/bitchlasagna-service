package com.kmd.bitchlasagna.LoginAuth.Repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public interface LoginDetailsRepo extends CrudRepository<LoginUsers,String> {

    //Below query is the native query where it was difficult to convert it to non native query
//    @Query(value = "SELECT LOGIN_ID,PASSWORD FROM LOGIN_USERS",nativeQuery = true)
    //Below non native query worked in  PostgresSQL
    @Query(value = "select loginDet.loginId,loginDet.password from LoginUsers loginDet")
    List<LoginUsers> getLoginFromRepo();
}
