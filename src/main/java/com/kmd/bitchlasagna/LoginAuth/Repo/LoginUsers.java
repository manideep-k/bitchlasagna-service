package com.kmd.bitchlasagna.LoginAuth.Repo;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Entity
@Table(name = "LOGIN_USERS")
@NoArgsConstructor

public class LoginUsers {

    @Id
    @Column(name="LOGIN_ID")
    @JsonView(LoginUsers.class)
    public String loginId;

    @Column(name="PASSWORD")
    @JsonView(LoginUsers.class)
    public String password;
}
