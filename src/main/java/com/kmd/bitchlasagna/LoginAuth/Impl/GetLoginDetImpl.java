package com.kmd.bitchlasagna.LoginAuth.Impl;

import com.kmd.bitchlasagna.LoginAuth.Dao.LoginDao;
import com.kmd.bitchlasagna.LoginAuth.Models.LoginDetailsModel;
import com.kmd.bitchlasagna.LoginAuth.Repo.LoginDetailsRepo;
import com.kmd.bitchlasagna.LoginAuth.Repo.LoginUsers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

@Component
public class GetLoginDetImpl implements LoginDao {


    private static final Log logger= LogFactory.getLog(GetLoginDetImpl.class);

    @Autowired
    LoginDetailsRepo loginDetailsRepo;

    @Autowired
    LoginDetailsModel loginDetailsModel;

    @Override
    public List<LoginUsers> getLoginDetails() throws Exception{

        try {
            List<LoginUsers> loginDetails = loginDetailsRepo.getLoginFromRepo();

            logger.info("Getting login details :::: " + loginDetails.toString());
            return loginDetails;
        }
        catch(Exception e) {
            throw new Exception(e);
        }
    }
}
