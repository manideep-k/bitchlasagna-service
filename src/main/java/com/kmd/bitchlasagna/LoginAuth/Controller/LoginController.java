package com.kmd.bitchlasagna.LoginAuth.Controller;

import com.kmd.bitchlasagna.LoginAuth.Impl.GetLoginDetImpl;
import com.kmd.bitchlasagna.LoginAuth.Repo.LoginUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LoginController {

    @Autowired
    GetLoginDetImpl loginDet;

    @ResponseBody
    @RequestMapping(value="/getLogin", method = RequestMethod.GET, headers="Accept=application/json")
    public List<LoginUsers> getLoginDetails() throws Exception{
        return loginDet.getLoginDetails();
    }

    @ResponseBody
    @RequestMapping("/api/hi")
    public String hi() {
        return "Test API :)";
    }
}
