import { Component, OnInit } from '@angular/core';
import { DashBoardService } from './dashboard.service';
import { Config } from 'protractor';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(public dashboardService:DashBoardService) { }
  random:any;

  ngOnInit() {
  }

  clickedRandom() { 
    this.dashboardService.getLoginDetails().subscribe(res => {
      console.log('=================');
      console.log(res);
    });

  }
}
